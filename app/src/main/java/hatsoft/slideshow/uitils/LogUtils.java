package hatsoft.slideshow.uitils;

import android.util.Log;

public class LogUtils {
    static final String LOG_TAG = "SLIDE_SHOW:";

    public static void logMemory() {
        Log.i("log", String.format("Total memory = %s", (int) (Runtime.getRuntime().totalMemory() / 1024)));
    }
    public static void log(String log) {
        Log.d(LOG_TAG ,log);
    }
    public static void logError(String text, Throwable t){
        Log.e(LOG_TAG, text, t);
    }

}

