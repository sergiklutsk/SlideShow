package hatsoft.slideshow.uitils;

import android.content.Context;
import android.widget.Toast;

import hatsoft.slideshow.MyApp;

public class ToasUtils {
    public static void success() {
        Toast.makeText(MyApp.getContext(),"success", Toast.LENGTH_LONG).show();
    }
    public static void success(String txt) {
        Toast.makeText(MyApp.getContext(), txt, Toast.LENGTH_LONG).show();
    }
    public static void success(Context context, String txt) {
        Toast.makeText(context,txt, Toast.LENGTH_LONG).show();
    }

    public static void error() {
        Toast.makeText(MyApp.getContext(),"error", Toast.LENGTH_LONG).show();
    }
    public static void error(String txt) {
        Toast.makeText(MyApp.getContext(), txt, Toast.LENGTH_LONG).show();
    }
    public static void error(Context context, String txt) {
        Toast.makeText(context,txt, Toast.LENGTH_LONG).show();
    }
}
