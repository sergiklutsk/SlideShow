package hatsoft.slideshow.uitils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;

public class ReadImage {
    public static Bitmap loadImage(String imgPath) {
        BitmapFactory.Options options; try { options = new BitmapFactory.Options(); options.inSampleSize = 2; Bitmap bitmap = BitmapFactory.decodeFile(imgPath, options); return bitmap; } catch(Exception e) { e.printStackTrace(); } return null; }

    public static Bitmap readImage() {
        File fullpath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "1.jpg");
        return BitmapFactory.decodeFile(fullpath.getAbsolutePath());
    }

    public static Bitmap readImage(File filename) {
        //File fullpath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), filename);
        return loadImage(filename.getAbsolutePath());
        //return BitmapFactory.decodeFile(filename.getAbsolutePath());
    }

    public static Bitmap readImage(File fullpath, String filename) {
        fullpath = new File(fullpath, filename);
        return BitmapFactory.decodeFile(fullpath.getAbsolutePath());
    }
    //File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"1.jpg");
}
