package hatsoft.slideshow.model.events;

import android.graphics.drawable.Drawable;

import hatsoft.slideshow.ui.utils.fragments.FragmentsAnimationId;
import hatsoft.slideshow.ui.utils.fragments.FragmentsId;

public interface FragmentChangedEvent {

    void changeFragment(FragmentsId fragmentsId, FragmentsAnimationId fragmentsAnimationId);
    void setFragment(FragmentsId fragmentsId);

    void setFragmentBitmap(FragmentsId fragmentsId, Drawable bitmap);
}
