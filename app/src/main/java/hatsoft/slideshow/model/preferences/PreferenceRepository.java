package hatsoft.slideshow.model.preferences;

import android.content.SharedPreferences;

public interface PreferenceRepository {
    /**
     * РЈСЃС‚Р°РЅРѕРІРёС‚СЊ Р·РЅР°С‡РµРЅРёРµ РІ SharedPreferences
     */
    void setValueSimple(String key, Object value);

    /**
     * Р’Р·СЏС‚СЊ Р·РЅР°С‡РµРЅРёРµ СЃ SharedPreferences
     */
    SharedPreferences getValueSimple();

    /**
     * РЈРґР°Р»РёС‚СЊ Р·РЅР°С‡РµРЅРёРµ РІ SharedPreferences
     */
    void removeSimple(String key);

    /**
     * РџСЂРѕРІРµСЂРёС‚СЊ СЃСѓР¶РµСЃС‚РІСѓРµС‚ Р»Рё Р·РЅР°С‡РµРЅРёРµ РІ SharedPreferences
     */
    boolean isValueExistSimple(String key);
}
