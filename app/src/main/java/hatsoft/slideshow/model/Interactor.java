package hatsoft.slideshow.model;

public interface Interactor {

    interface interactorSettings {

        void directoryChange();

        String getDirectory();
    }
}
