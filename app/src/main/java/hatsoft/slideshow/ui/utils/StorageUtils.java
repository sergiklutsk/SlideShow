package hatsoft.slideshow.ui.utils;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;

import hatsoft.slideshow.model.preferences.PreferenceRepository;

public class StorageUtils implements PreferenceRepository {
    private static ArrayList<File> FilesListArray = new ArrayList<>();

    void StorageUtils() {
    }

    public Drawable getImage(int number) {
        Drawable bm;
        bm = Drawable.createFromPath(String.valueOf(FilesListArray.get(number)));
        return (bm);
    }

    public static ArrayList<File> listFilesWithSubFolders(String directory_path) {
        File dir = Environment.getExternalStoragePublicDirectory(directory_path);
        for (File file : dir.listFiles()) {
            if (file.isDirectory())
                FilesListArray.addAll(listFilesWithSubFolders(directory_path)); //Environment.DIRECTORY_PICTURES
            else FilesListArray.add(file);
        }
        return FilesListArray;
    }

    @Override
    public void setValueSimple(String key, Object value) {

    }

    @Override
    public SharedPreferences getValueSimple() {
        return null;
    }

    @Override
    public void removeSimple(String key) {

    }

    @Override
    public boolean isValueExistSimple(String key) {
        return false;
    }
    //    public static String pathSdStorage() {
//        File sdDir;
//        String sdState = android.os.Environment.getExternalStorageState();
//        if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
//            sdDir = android.os.Environment.getExternalStorageDirectory();
//        }
//        return sdDir.toString();
//    }
}
