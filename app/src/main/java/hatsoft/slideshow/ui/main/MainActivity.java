package hatsoft.slideshow.ui.main;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import hatsoft.slideshow.R;
import hatsoft.slideshow.ui.base.BaseActivity;
import hatsoft.slideshow.ui.settings.SettingsFragment;
import hatsoft.slideshow.ui.slides.SlideOneFragment;
import hatsoft.slideshow.ui.slides.SlideTwoFragment;
import hatsoft.slideshow.ui.utils.fragments.FragmentsAnimationId;
import hatsoft.slideshow.ui.utils.fragments.FragmentsId;

public class MainActivity extends BaseActivity implements
        SlideOneFragment.OnFragmentInteractionListener,
        SlideTwoFragment.OnFragmentInteractionListener,
        SettingsFragment.OnFragmentInteractionListener, MainActivityView.View {

    MainActivityPresenter presenter = new MainActivityPresenter();

    private static final int FRAGMENTS_CONTEINER = R.id.frgHolder;
    private SlideOneFragment imageFragmentOne;
    private SlideTwoFragment imageFragmentTwo;
    private SettingsFragment settingsFragment;
    private ImageButton buttonExit;
    private FrameLayout mContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        presenter.bindView(this);

        imageFragmentOne = new SlideOneFragment();
        imageFragmentTwo = new SlideTwoFragment();
        settingsFragment = new SettingsFragment();

        buttonExit = findViewById(R.id.btn_exit);
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(FragmentsId.FIRST_FRAGMENT, FragmentsAnimationId.RIGHT_TO_LEFT);
                presenter.startCountDownTimer();
            }
        });

        mContainer = findViewById(R.id.frgHolder);
        mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.stopCountDownTimer();
                changeFragment(FragmentsId.SETTINGS_FRAGMENT, FragmentsAnimationId.RIGHT_TO_LEFT);
            }
        });

        presenter.onViewCreated();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unbindView();
    }

    @Override
    public void changeFragment(FragmentsId fragmentsId, FragmentsAnimationId fragmentsAnimationId) {
        changeFragmentWithAnimation(getSupportFragmentManager(), getFragmentById(fragmentsId), FRAGMENTS_CONTEINER, fragmentsAnimationId);
    }

    @Override
    public void setFragment(FragmentsId fragmentsId) {
        setFragment(getSupportFragmentManager(), getFragmentById(fragmentsId), FRAGMENTS_CONTEINER);
    }

    @Override
    public void setFragmentBitmap(FragmentsId fragmentsId, Drawable bitmap) {
        switch (fragmentsId) {
            case FIRST_FRAGMENT:
                imageFragmentOne.setBitmap_image(bitmap);
            case SECOND_FRAGMENT:
                imageFragmentTwo.setBitmap_image(bitmap);
        }
    }

    private Fragment getFragmentById(FragmentsId fragmentsId) {
        switch (fragmentsId) {
            case FIRST_FRAGMENT:
                return imageFragmentOne;
            case SECOND_FRAGMENT:
                return imageFragmentTwo;
            case SETTINGS_FRAGMENT:
                return settingsFragment;
            default:
                return settingsFragment;
        }
    }

    @Override
    public void closeApplication() {

    }
}
