package hatsoft.slideshow.ui.main;

import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;

import hatsoft.slideshow.ui.utils.StorageUtils;
import hatsoft.slideshow.ui.utils.fragments.FragmentsAnimationId;
import hatsoft.slideshow.ui.utils.fragments.FragmentsId;

public class MainActivityPresenter implements MainActivityView.Presenter {

    private MainActivityView.View mView;
    private FragmentsId mSelectedPage;

    private static final long START_TIME_IN_MILLIS = 10000;
    public static boolean current_fragment_is_first = true;
    public static int imageCounter = 0;
    public static int effectCounterFragmentFirst = 0;
    public static int effectCounterFragment = 0;
    private ArrayList<File> fileArrayList;
    private CountDownTimer mCountDownTimer;
    private boolean mTimerRunning;
    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;
    private Drawable bitmap_image;

    public MainActivityPresenter() {
    }

    @Override
    public void bindView(MainActivityView.View view) {
        mView = view;
    }

    @Override
    public void unbindView() {
        mView = null;
    }

    @Override
    public void onViewCreated() {
        fileArrayList = StorageUtils.listFilesWithSubFolders(Environment.DIRECTORY_PICTURES);
        bitmap_image = Drawable.createFromPath(String.valueOf(fileArrayList.get(imageCounter)));
        mView.setFragmentBitmap(FragmentsId.FIRST_FRAGMENT, bitmap_image);
        mView.setFragment(FragmentsId.FIRST_FRAGMENT);

        startTimer();
    }

    private void startTimer() {

        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
            }

            @Override
            public void onFinish() {

                prepareNextFragment();
                mTimerRunning = false;
                mTimeLeftInMillis = START_TIME_IN_MILLIS;
                effectCounterFragment++;
                if (effectCounterFragment > 3) effectCounterFragment = 0;

                if (current_fragment_is_first) {
                    switch (effectCounterFragment) {
                        case 0:
                            mView.changeFragment(FragmentsId.SECOND_FRAGMENT, FragmentsAnimationId.LEFT_TO_RIGHT);
                            break;
                        case 1:
                            mView.changeFragment(FragmentsId.SECOND_FRAGMENT, FragmentsAnimationId.ROTATE_IN);
                            break;
                        case 2:
                            mView.changeFragment(FragmentsId.SECOND_FRAGMENT, FragmentsAnimationId.RIGHT_TO_LEFT);
                            break;
                        case 3:
                            mView.changeFragment(FragmentsId.SECOND_FRAGMENT, FragmentsAnimationId.SCALE_IN);
                            break;
                    }
                    current_fragment_is_first = false;
                } else {
                    switch (effectCounterFragment) {
                        case 0:
                            mView.changeFragment(FragmentsId.FIRST_FRAGMENT, FragmentsAnimationId.LEFT_TO_RIGHT);
                            break;
                        case 1:
                            mView.changeFragment(FragmentsId.FIRST_FRAGMENT, FragmentsAnimationId.ROTATE_IN);
                            break;
                        case 2:
                            mView.changeFragment(FragmentsId.FIRST_FRAGMENT, FragmentsAnimationId.RIGHT_TO_LEFT);
                            break;
                        case 3:
                            mView.changeFragment(FragmentsId.FIRST_FRAGMENT, FragmentsAnimationId.SCALE_IN);
                            break;
                    }
                    current_fragment_is_first = true;
                }
                startTimer();
            }
        }.start();
        mTimerRunning = true;
    }

    public void prepareNextFragment() {

        if (imageCounter > fileArrayList.size()) {
            imageCounter = 0;
        } else {
            imageCounter++;
        }
        bitmap_image = Drawable.createFromPath(String.valueOf(fileArrayList.get(imageCounter)));

        if (current_fragment_is_first) {
            mView.setFragmentBitmap(FragmentsId.FIRST_FRAGMENT, bitmap_image);
        } else {
            mView.setFragmentBitmap(FragmentsId.SECOND_FRAGMENT, bitmap_image);
        }
    }

    public void stopCountDownTimer() {
        mCountDownTimer.cancel();
    }

    public void startCountDownTimer() {
        mCountDownTimer.start();
    }

    @Override
    public void closeApplication() {

    }
}
