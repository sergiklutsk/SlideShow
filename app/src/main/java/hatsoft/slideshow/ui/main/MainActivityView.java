package hatsoft.slideshow.ui.main;

import hatsoft.slideshow.model.events.FragmentChangedEvent;
import hatsoft.slideshow.ui.base.BaseView;

public interface MainActivityView {

    interface View extends BaseView.RootView, FragmentChangedEvent {
        void closeApplication();
    }

    interface Presenter extends BaseView.Presenter<View> {
        void closeApplication();
    }
}
