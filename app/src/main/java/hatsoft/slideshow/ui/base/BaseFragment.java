package hatsoft.slideshow.ui.base;

import android.content.Context;
import android.view.View;

import androidx.fragment.app.Fragment;

import hatsoft.slideshow.ui.utils.AnimationViewUtils;

public class BaseFragment extends Fragment {

    public void showView(Context context, View view){
        AnimationViewUtils.showView(context, view);
    }

    public void hideView(Context context, View view){
        AnimationViewUtils.hideView(context, view);
    }

    public void changeViews(Context context, View newView, View oldView){
        AnimationViewUtils.changeViews(newView, oldView, context);
    }

}
