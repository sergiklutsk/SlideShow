package hatsoft.slideshow.ui.base.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import hatsoft.slideshow.ui.main.MainActivity;

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Toast toast = Toast.makeText(context.getApplicationContext(),
                    "SLIDESHOW: onReceive !!!", Toast.LENGTH_LONG);
            toast.show();
            Log.d("SLIDESHOW", ": onReceive !!!");
            Intent i = new Intent(context, MainActivity.class);  //MyActivity can be anything which you want to start on bootup...
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
//        PreferenceRepository pref = new PreferenceRepositoryImpl(MyApp.getContext());
//        if (pref.getValueSimple().getBoolean(PreferenceRepositoryImpl.IS_RUN, false)){
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                context.startForegroundService(intent);
//            } else context.startService(intent);
//        } else context.stopService(intent);
//    }
    }
}