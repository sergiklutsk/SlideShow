package hatsoft.slideshow.ui.settings;

import hatsoft.slideshow.model.Interactor;

public class SettingsFragmentPresenter implements SettingsFragmentView.Presenter {

    String slides_path;
    private Interactor.interactorSettings mInteractorSettings;
    private SettingsFragmentView.View mView;

    public SettingsFragmentPresenter(Interactor.interactorSettings interactorSettings) {
        mInteractorSettings = interactorSettings;
    }

    @Override
    public void bindView(SettingsFragmentView.View view) {
        mView = view;
    }

    @Override
    public void unbindView() {
        mView = null;
    }

    @Override
    public void onViewCreated() {
        slides_path = mInteractorSettings.getDirectory();
    }
}
