package hatsoft.slideshow;

public interface Counter {
    int getCounter();

    void incrementCounter();
}
